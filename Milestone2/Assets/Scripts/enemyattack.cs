﻿using UnityEngine;
using System.Collections;

public class enemyattack : MonoBehaviour
{
    public float speed;
    private GameObject Enemy;
    private GameObject Player;

    private Vector3 Target;
    private Vector3 Origin;

    public GameObject Prefab;
    private float Speed;



    // Use this for initialization
    void Start()
    {
        Enemy = GameObject.FindGameObjectWithTag("Enemy");
        Player = GameObject.FindGameObjectWithTag("Player");

        Target = Player.transform.position;
        Origin = Enemy.transform.position;

        InvokeRepeating("Fire", 0.1f, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
     
    }
    void Fire()
    {
        GameObject clone;
        clone = Instantiate(Prefab, transform.position, Player.transform.rotation) as GameObject;
        clone.GetComponent<Rigidbody2D>().velocity = (Player.transform.position - transform.position);

    }
}