﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
    private Vector3 MousePosition;
    public float Speed;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        MousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MousePosition.z = transform.position.z;
        transform.position = Vector3.MoveTowards(transform.position, MousePosition, 100.0f * Time.deltaTime);

    }
}
