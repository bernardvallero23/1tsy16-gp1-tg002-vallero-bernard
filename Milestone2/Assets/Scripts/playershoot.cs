﻿using UnityEngine;
using System.Collections;

public class playershoot : MonoBehaviour {

    public float speed;
    private GameObject Enemy;
    private GameObject Player;

    private Vector3 Target;
    private Vector3 Origin;

    public GameObject Prefab;
    private float Speed;

    public float Firerate;
    private float Rate;

    // Use this for initialization
    void Start () {

        Enemy = GameObject.FindGameObjectWithTag("Enemy");
        Player = GameObject.FindGameObjectWithTag("Player");

        Target = Enemy.transform.position;
        Origin = Player.transform.position;

        InvokeRepeating("Fire", 0.1f, 0.5f);

   

}
	
	// Update is called once per frame
	void Update () {
	
	}

    void Fire()
    {
        GameObject clone;
        clone = Instantiate(Prefab, transform.position, Enemy.transform.rotation) as GameObject;
        clone.GetComponent<Rigidbody2D>().velocity = (Enemy.transform.position - transform.position);

    }
}
