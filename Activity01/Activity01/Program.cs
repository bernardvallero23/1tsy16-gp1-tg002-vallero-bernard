﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Activity01
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> items = new List<string>();
            items.Add("sword");
            items.Add("knife");
            items.Add("gun");
            items.Add("stick");

            string searchterm = Console.ReadLine();
            bool item = items.Contains(searchterm);


            if (item == true)
            {   
                Console.WriteLine("The item is in the list");
            }
            else
            {
                Console.WriteLine("The items is not in the list");
            }

            Console.ReadKey();
          

        }
       
    }
}
