﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Player : Commander
    {
        /// <summary>
        /// Get player's input. Player can only either Play or Discard.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            //Display cards
            DisplayCards();

            Console.WriteLine("\nChoose your move.");
            Console.WriteLine("a: Play | b: Discard");

            //get player input
            char input = char.Parse(Console.ReadLine());

            if (input == 'a')
            {
                Fight(opponent);
            }
            else if (input == 'b')
            {
                Discard();
            }
            else
            {
                Console.WriteLine("Invalid key");
            }

            Cards.Add(Discard());
        }


        /// <summary>
        /// Show a list of cards to choose from. If there are 2 cards of the same type (eg. Warrior), only show one of each type.
        /// </summary>
        /// <returns></returns>
        public override Card PlayCard()
        {
            Console.WriteLine("\nChoose one card.");
            Console.WriteLine("a: Warrior | b: Mage | c: Assassin");
            char input = char.Parse(Console.ReadLine());
            string cardname = "";
            if (input == 'a')
            {
                cardname = "Warrior";
            }
            else if (input == 'b')
            {
                cardname = "Mage";
            }
            else if (input == 'c')
            {
                cardname = "Assassin";
            }
            else
            {
                cardname = "";
            }


            Card returncard = null;

            for (int i = 0; i < Cards.Count; i++)
            {
                if (Cards[i].Type == cardname)
                {
                    returncard = Cards[i];
                    Cards.RemoveAt(i);
                }
            }
            return returncard;
        }
    }

}       
         
