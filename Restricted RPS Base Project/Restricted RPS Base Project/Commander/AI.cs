﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class AI : Commander
    {
        /// <summary>
        /// 30% chance to discard, 70% chance to play a card.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            if (RandomHelper.Chance(0.7f))
            {
                PlayCard();
            }
            else
            {
                Discard();
            }
        }

        public override Card PlayCard()
        {
            int r = RandomHelper.Range(Cards.Count);
            Card returncard = Cards[r];
            Cards.RemoveAt(r);
            return returncard;
        }
    }
}
