﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Warrior : Card
    {
        public Warrior() //constructor
        {
            Type = "Warrior";
        }
        public override FightResult Fight(Card opponentCard)
        {
            if (opponentCard.Type == "Mage")
            {
                Console.WriteLine("You lose");
                return FightResult.Lose;
            }
            else if (opponentCard.Type == "Assassin")
            {
                Console.WriteLine("You Win");
                return FightResult.Win;
            }
            return FightResult.Draw;
            
        }
    }
}
