﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Mage : Card
    {
        public Mage() //constructor
        {
            Type = "Mage";
        }
        public override FightResult Fight(Card opponentCard)
        {
            if (opponentCard.Type == "Warrior")
            {
                Console.WriteLine("You win");
                return FightResult.Win;
            }
            else if (opponentCard.Type == "Assassin")
            {
                Console.WriteLine("You lose");
                return FightResult.Lose;
            }
            return FightResult.Draw;
        }
    }
}
