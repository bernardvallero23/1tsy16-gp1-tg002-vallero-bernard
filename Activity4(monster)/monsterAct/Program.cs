﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace monsterAct
{

    class Program
    {

       
        static void PrintMonster(string[] monster)
        {
            for (int i = 0; i < monster.Length; ++i)
                Console.WriteLine(string.Format("Monster[{0}] {1}", i, monster[i]));
            
        }
      
        
        static void Main(string[] args)
        {
            int monsterNum = 0;
            char choice;
            /// Number of monster
            do
            {
                Console.Write("Enter number of monster (ranges 5-10)");
                monsterNum = Convert.ToInt32(Console.ReadLine());
            }
            while (monsterNum < 5 || monsterNum > 10);

            string[] monster = new string[monsterNum];
            PrintMonster(monster);

            Console.WriteLine("============");
            Console.WriteLine("a.Add b.Remove c.Print the Database");
            choice = Convert.ToChar(Console.ReadLine());
            if (choice == 'a' || choice == 'A')
            {
                List<string> nameList = new List<string>();
                {
                    Console.Write("Enter Name: ");
                    nameList.Add(Console.ReadLine().ToString());
                    foreach (String names in nameList)
                    {
                        Console.WriteLine(names);
                    }
                    PrintMonster(monster);
                }

               
            }
            if (choice == 'b' || choice == 'B')
            {
                List<string> nameList = new List<string>();
                Console.Write("Remove a Name Line #: ");
                int indexToRemove = Convert.ToInt32(Console.ReadLine());
                nameList.RemoveAt(indexToRemove - 1);
            
            }
            if (choice == 'c' || choice == 'C')
            {
                PrintMonster(monster);


            }
                        


            
            Console.ReadKey();

           
        }

      

    }
}
