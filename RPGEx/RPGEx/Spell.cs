﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    struct Spell
    {
        public string Name;
        public Range DamageRange;
        public int MagicPointsRequired;

    }
}
