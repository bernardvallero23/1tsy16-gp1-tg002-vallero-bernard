﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{

    public class Player
    {
        public string Name { get; private set; }

        //SPELLS
        Spell getLucentBeam()
        {
            Spell spells;
            spells.Name = "LucentBeam";
            spells.MagicPointsRequired = 8;
            spells.DamageRange.Low = 5;
            spells.DamageRange.High = 8;

            return spells;
        }

        Spell getVoodoo()
        {
            Spell spells;
            spells.Name = "Voodoo";
            spells.MagicPointsRequired = 10;
            spells.DamageRange.Low = 7;
            spells.DamageRange.High = 14;

            return spells;
        }

        Spell getEpicenter()
        {
            Spell spells;
            spells.Name = "Epicenter";
            spells.MagicPointsRequired = 20;
            spells.DamageRange.Low = 15;
            spells.DamageRange.High = 25;

            return spells;
        }
        Spell getFrozebite()
        {
            Spell spells;
            spells.Name = "FrozeBite";
            spells.MagicPointsRequired = 25;
            spells.DamageRange.Low = 15;
            spells.DamageRange.High = 27;

            return spells;
        }
        // Constructor
        public Player()
        {
            Name = "Default";
            className = "Default";
            accuracy = 0;
            hitPoints = 0;
            maxHitPoints = 0;
            expPoints = 0;
            goldPoints = 0;
            nextLevelExp = 0;
            level = 0;
            Armor = 0;
            weapon = new Weapon("Default Weapon Name", 0, 0);
        }

        public void CreateClass()
        {
            Console.WriteLine("CHARACTER CLASS GENERATION");
            Console.WriteLine("==========================");

            // Input character's name.
            Console.WriteLine("Enter your character's name: ");
            Name = Console.ReadLine();

            Console.WriteLine("Please select a character class number...");
            Console.WriteLine("1)Fighter 2)Wizard 3)Cleric 4)Thief : ");

            int characterNum = 1;

            characterNum = Convert.ToInt32(Console.ReadLine());

            switch (characterNum)
            {
                case 1: //Fighter
                    className = "Fighter";
                    accuracy = 10;
                    hitPoints = 50;
                    maxHitPoints = 20;
                    expPoints = 0;
                    goldPoints = 0;
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 4;
                    weapon = new Weapon("Long Sword", 1, 8);
                    spells.MagicPointsRequired = 8;
                    spells.Name = "Lucent Beam";
                    magicPoints = 25;
                    maxmagichitPoints = 10;
                    getLucentBeam();
                    
                    break;
                case 2: //Wizard
                    className = "Wizard";
                    accuracy = 5;
                    hitPoints = 55;
                    maxHitPoints = 10;
                    expPoints = 0;
                    goldPoints = 0;
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 1;
                    weapon = new Weapon("Staff", 1, 4);
                    spells.MagicPointsRequired = 10;
                    spells.Name = "Voodoo";
                    magicPoints = 25;
                    maxmagichitPoints = 11;
                    getVoodoo();

                    break;
                case 3:
                    className = "Cleric";
                    accuracy = 8;
                    hitPoints = 60;
                    maxHitPoints = 15;
                    expPoints = 0;
                    goldPoints = 0;
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 3;
                    weapon = new Weapon("Flail", 1, 6);
                    spells.MagicPointsRequired = 20;
                    spells.Name = "Epicenter";
                    magicPoints = 25;
                    maxmagichitPoints = 20;
                    getEpicenter();
                    break;
                default: //
                    className = "Thief";
                    accuracy = 7;
                    hitPoints = 65;
                    maxHitPoints = 12;
                    expPoints = 0;
                    goldPoints = 0;
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 2;
                    weapon = new Weapon("Dagger", 1, 6);
                    spells.MagicPointsRequired = 25;
                    spells.Name = "Froze Bite";
                    magicPoints = 25;
                    maxmagichitPoints = 25;
                    getFrozebite();
                    break;
            }

        }
        
        public void Race()
        {
            Console.WriteLine("RACE SELECTION");
            Console.WriteLine("=====================");

            Console.WriteLine("Select your race");
            Console.WriteLine("1.Elf 2.Stone Giant 3.Dragon 4.Frost Giant:");

            int raceNum = 1;
            raceNum = Convert.ToInt32(Console.ReadLine());
            switch(raceNum)
            {
                case 1: //ELF
                hitPoints  -= 3;
                maxHitPoints += 3;

                break;

                case 2: //STONE GIANT
                hitPoints -= 4;
                maxHitPoints -= 3;
                break;

                case 3: //DRAGON
                hitPoints += 1;
                maxHitPoints += 3;
                break;

                case 4: //FROST GIANT
                hitPoints -= 5;
                maxHitPoints -= 3;
                break;

            }

            hitPoints = maxHitPoints;

        }

        public bool isDead { get { return hitPoints <= 0; } }
        public int Armor { get; private set; }
        public bool Attack(Monster monsterTarget)
        {
            int selection = 1;
            
            Console.Write("1) Attack, 2) Run 3) Cast A Spell:");
            selection = Convert.ToInt32(Console.ReadLine());
            switch (selection)
            {
                case 1:
                    Console.WriteLine("You attack an " + monsterTarget.Name + " with a " + weapon.Name);
                    if (RandomHelper.Random(0, 20) < accuracy)
                    {
                        int damage = RandomHelper.Random(weapon.DamageRange);
                        int totalDamage = damage - monsterTarget.Armor;
                        if (totalDamage <= 0)
                        {
                            Console.WriteLine("Your attack failed to penetrate the armor");
                        } else
                        {
                            Console.WriteLine("You attack for " + totalDamage.ToString() + " damage!");
                            monsterTarget.TakeDamage(totalDamage);
                        }
                    }
                    else
                    {
                        Console.WriteLine("You miss!");
                    }
                    break;
                case 2:
                    // 25% chance of being able to run
                    int roll = RandomHelper.Random(1, 4);
                    if (roll == 1)
                    {
                        Console.WriteLine("You run away!");
                        return true; //<-- Return out of the function
                    }
                    else
                    {
                        Console.WriteLine("You could not escape!");
                    }
                    break;
                case 3:
                {
                    Console.WriteLine("Your Spells:" + spells.Name.ToString());

                    Console.WriteLine("Your Magic Points:" + magicPoints.ToString());
                    if(spells.MagicPointsRequired <= magicPoints)
                    {
                        magicPoints -= spells.MagicPointsRequired;
                        int damage = RandomHelper.Random(spells.DamageRange);
                        int totalDamage = damage - monsterTarget.Armor;
                        Console.WriteLine("You cast:" + spells.Name.ToString());
                        Console.WriteLine("You attack for " + totalDamage.ToString() + " damage!");
                        monsterTarget.TakeDamage(totalDamage);
                        return false;

                    }
                    else
                    {
                        Console.WriteLine("You dont have enough magic points to use this spell");
                    }


                    break;
                }
            }

            return false;
        }


        public void TakeDamage(int damage)
        {
            hitPoints -= damage;
            if (hitPoints < 0)
            {
                hitPoints = 0;
            }
        }

        public void LevelUp()
        {
            if (expPoints >= nextLevelExp)
            {
                Console.WriteLine("You gained a level!");

                // Increment level.
                level++;

                // Set experience points requard for next level
                nextLevelExp = level * level * 1000;

                // Increase stats randomly
                accuracy += RandomHelper.Random(1, 3);
                maxHitPoints += RandomHelper.Random(2, 6);
                Armor += RandomHelper.Random(1, 2);

                // Give the player full hitpoints when they level up.
                hitPoints = maxHitPoints;
                magicPoints = maxmagichitPoints;
            }
        }
      
        public void Rest()
        {
            Console.WriteLine("Resting...");

            hitPoints = maxHitPoints;

            // TODO: Modify the function so that random enemy enounters
            // are possible when resting


        }

        public void ViewStats()
        {
            Console.WriteLine("PLAYER STATS");
            Console.WriteLine("============");
            Console.WriteLine();

            Console.WriteLine("Name             = " + Name);
            Console.WriteLine("Class            = " + className);
            Console.WriteLine("Accuracy         = " + accuracy.ToString());
            Console.WriteLine("Hitpoints        = " + hitPoints.ToString());
            Console.WriteLine("MaxHitpoints     = " + maxHitPoints.ToString());
            Console.WriteLine("XP               = " + expPoints.ToString());
            Console.WriteLine("XP to next level = " + nextLevelExp.ToString());
            Console.WriteLine("Level            = " + level.ToString());
            Console.WriteLine("Armor            = " + Armor.ToString());
            Console.WriteLine("Weapon Name      = " + weapon.Name);
            Console.WriteLine("Weapon Damage    = " + weapon.DamageRange.Low.ToString() + "-" + weapon.DamageRange.High.ToString());
            Console.WriteLine("Current Gold     = " + goldPoints.ToString());
            Console.WriteLine("Current MagicPoints=" + magicPoints.ToString());
            Console.WriteLine();
            Console.WriteLine("END PLAYER STATS");
            Console.WriteLine("================");
            Console.WriteLine();
        }
        
        public void Victory(int xp)
        {
            Console.WriteLine("You won the battle!");
            Console.WriteLine("You win " + xp.ToString() + " experience points!");
            expPoints += xp;
        }

        public void GameOver()
        {
            Console.WriteLine("You died in battle...");
            Console.WriteLine();
            Console.WriteLine("==========================");
            Console.WriteLine("GAME OVER!");
            Console.WriteLine("==========================");
            Console.ReadLine();
        }

        public void DisplayHitPoints()
        {
            Console.WriteLine(Name + "'s hitpoints = " + hitPoints.ToString());
        }

        //ADD GOLD
         public void addGold(int golds)
        {
            Console.WriteLine("You win " + golds.ToString() + " gold!! ");
            goldPoints += golds;
            
        }
        //DeductGold
        public void deductGold(int golds)
         {
             Console.WriteLine("Your current gold " + golds.ToString());
             goldPoints -= golds;
         }
      
        private string className;
        private int hitPoints;
        private int maxHitPoints;
        private int expPoints;
        private int nextLevelExp;
        private int level;
        private int goldPoints;
        private int accuracy;
        private int magicPoints;
        private int maxmagichitPoints;
        private Spell spells;
        private Weapon weapon;
    }
}
