﻿using UnityEngine;
using System.Collections;

public class SharkMovement : MonoBehaviour {

    private Vector3 SharkMove;
    public float Speed;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        SharkMove = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        SharkMove.z = transform.position.z;
        transform.position = Vector3.MoveTowards(transform.position, SharkMove, 20.0f * Time.deltaTime);
    }
}
