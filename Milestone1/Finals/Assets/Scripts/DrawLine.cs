﻿using UnityEngine;
using System.Collections;

public class DrawLine : MonoBehaviour {
    public GameObject mark;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
            GameObject ship = GameObject.FindGameObjectWithTag("Player");
            GetComponent<LineRenderer>().SetPosition(0, mark.transform.position);
            GetComponent<LineRenderer>().SetPosition(1, ship.transform.position);
        
    }

}

