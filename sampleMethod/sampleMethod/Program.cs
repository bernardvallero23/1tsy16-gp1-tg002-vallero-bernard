﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace methods
{

    public class Circle
    {
        private static double radius = 1.0;
        private static string color = "red";

        static void circle()
        {
            radius = 1.0;
            color = "red";
        }

        // setters and getters
        public void SetRadius(double newValue)
        {
            radius = newValue;
        }
        public void SetColor(string newColor)
        {
            color = newColor;
        }

        public double GetRadius()
        {
            return radius;
        }

        public string GetColor()
        {
            return color;
        }

  

    }


    class Program
    {
        static void Main(string[] args)
        {
            char radius;
            string color;
            Circle circle = new Circle();
            Console.WriteLine(circle.GetRadius());
            Console.WriteLine(circle.GetColor());
            Console.WriteLine("========================");

            Console.WriteLine("User input radius:");
            radius = Convert.ToChar(Console.ReadLine());
            Console.WriteLine("User input color:");
            color = Convert.ToString(Console.ReadLine());
            Console.WriteLine("========================");
            Console.WriteLine("new radius:");
            Console.WriteLine(radius);
            Console.WriteLine("new color:");
            Console.WriteLine(color);

            Console.ReadKey();
            
        }
    }

}
