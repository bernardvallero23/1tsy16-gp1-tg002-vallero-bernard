﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{

    private Vector3 FollowCamera;


    // Use this for initialization
    void Start()
    {



    }

    // Update is called once per frame
    void Update()
    {
        FollowCamera = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        FollowCamera.z = transform.position.z;
        transform.position = Vector3.MoveTowards(transform.position, FollowCamera, 100.0f * Time.deltaTime);

    }
}