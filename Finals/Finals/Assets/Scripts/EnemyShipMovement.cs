﻿using UnityEngine;
using System.Collections;

public class EnemyShipMovement : MonoBehaviour {

    private Vector3 Shipmovement;
    public float Speed;

    // Use this for initialization
    void Start () {

    
	
	}
	
	// Update is called once per frame
	void Update () {
        Shipmovement = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Shipmovement.z = transform.position.z;
        transform.position = Vector3.MoveTowards(transform.position, Shipmovement, 20.0f * Time.deltaTime);

    }
}
