﻿using UnityEngine;
using System.Collections;

public class Cursor : MonoBehaviour {

    private Vector3 MousePosition;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        MousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MousePosition.z = transform.position.z;
        transform.position = Vector3.MoveTowards(transform.position, MousePosition, 1000.0f * Time.deltaTime);

    }
}
